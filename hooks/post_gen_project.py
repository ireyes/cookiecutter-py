import os
import re
import shlex
import shutil
import subprocess
import sys
from os import path

import click

PACKAGES = []
CONDA_CMD = '{conda} create -n {name} python={version} {packages} --yes'
CONDA_EXPORT = '{conda} env export -n {name}'

ENV_PATH_REGEX = re.compile(r'prefix:\s+(?P<path>.*)')


def is_executable(executable):
    return False if shutil.which(executable) is None else True


def which(program):
    fpath, fname = path.split(program)
    if fpath:
        if is_executable(program):
            return program
    else:
        for path_ in os.environ["PATH"].split(os.pathsep):
            exe_file = path.join(path_, program)
            if is_executable(exe_file):
                return exe_file


def launch_cmd(cmd):
    return subprocess.run(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)


conda_path = which('conda')

if conda_path is not None:
    click.echo('')
    click.echo(' Conda found in {}'.format(conda_path))
    if click.confirm('Do you want to create a conda environment for the project?', default=True):

            env_name = click.prompt('Give a name to your environment', default='{{cookiecutter.package_name}}')
            python_version = click.prompt('Indicate the Python version', default=None)

            packages = PACKAGES[:]

            if "{{cookiecutter.command_line_name}}":
                packages = ['click'] + packages  # keep alphabetical order

            conda_create_cmd = CONDA_CMD.format(conda=conda_path, name=env_name, version=python_version, packages=' '.join(packages))

            click.echo('$ ' + conda_create_cmd)

            return_code = os.system(conda_create_cmd)  # use system to show the output to the user

            if return_code != 0:
                print('WARNING: Cannot create conda environment')
                # sys.exit(1)  Do not exit, is not crucial
            else:

                # Update requirements file
                with open('requirements.txt', 'w') as fd:
                    fd.write('\n'.join(packages))

                # Find the path to the environment
                conda_export_cmd = CONDA_EXPORT.format(conda=conda_path, name=env_name)
                try:
                    cp = launch_cmd(conda_export_cmd)
                except subprocess.CalledProcessError as e:
                    print('ERROR finding conda path')
                    sys.exit(1)
                else:
                    output = cp.stdout.decode('utf-8')
                    match = ENV_PATH_REGEX.search(output)
                    if match is None:
                        print('ERROR getting environment path')
                        sys.exit(1)
                    env_path = match.group('path')

                    pip_path = path.join(env_path, 'bin', 'pip')
                    pip_install_cmd = ' '.join([pip_path, 'install', '-e', os.getcwd()])
                    click.echo('Installing the project in editable mode in the environment...')
                    click.echo('$ ' + pip_install_cmd)
                    try:
                        launch_cmd(pip_install_cmd)
                    except subprocess.CalledProcessError:
                        print('ERROR installing project in editable mode')
                        sys.exit(1)
