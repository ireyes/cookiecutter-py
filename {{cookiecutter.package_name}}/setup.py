from os import path
from setuptools import setup, find_packages


VERSION = '0.1'
DESCRIPTION = ''


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='{{cookiecutter.package_name}}',
    version=VERSION,
    description=DESCRIPTION,
    packages=find_packages(),
    install_requires=required,
    {%- if cookiecutter.command_line_name %}
    entry_points={
        'console_scripts': [
            '{{cookiecutter.command_line_name}} = {{cookiecutter.package_name}}.cli:cli',
        ]
    }
    {%- endif %}
)