{% if cookiecutter.command_line_name %}

import logging
from logging import config as logging_config

import click


logger = logging.getLogger('{{cookiecutter.package_name}}')


CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


class LogFilter(logging.Filter):

    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO)


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('--debug', default=False, is_flag=True, help='Give more output (verbose)')
@click.version_option()
def cli(debug):
    """{{cookiecutter.package_name}} help"""

    log_conf = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'fmt': {
                'format': '%(asctime)s %(name)s %(levelname)s -- %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'filters': {
            'info': {
                '()': LogFilter
            }
        },
        'handlers': {
            'out': {
                'class': 'logging.StreamHandler',
                'formatter': 'fmt',
                'level': 'DEBUG',
                'stream': 'ext://sys.stdout',
                'filters': ['info']
            },
            'err': {
                'class': 'logging.StreamHandler',
                'formatter': 'fmt',
                'level': 'WARNING',
                'stream': 'ext://sys.stderr',
            }
        },
        'loggers': {
            '{{cookiecutter.package_name}}': {
                'level': logging.DEBUG if debug else logging.INFO

            }
        },
        'root': {
            'level': 'WARNING',
            'handlers': ['out', 'err']
        }
    }
    logging_config.dictConfig(log_conf)
    logger.debug('Debug mode enabled')


if __name__ == "__main__":
    cli()

{%- endif %}