
cookiecutter-py
===============

Template for a Python project::

   |- <package_name>/
   |
   |  |- <package_name>/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |
   |- .gitignore
   |- LICENSE  # emtpy
   |- MANIFEST.in
   |- README.rst
   |- requirements.txt
   |- setup.py


Optionally, you can enable a **command line interface**
(with the `click <http://click.pocoo.org>`_ library)
by indicating a name when asking for the ``command line name``.
If you leave that option empty, the command line will not be enabled.

If conda is installed in your system
(and the script finds it)
you will aso be asked whether you want to have a
**conda environment** for your project.
If so, you will be asked for the name of the environment
and the version of Python.
An environment with that Python version and some libraries
(*matplotlib*, *numpy*, *pandas* and *scipy*) will be created
and the project installed within it in **editable** mode.

.. warning:: If you want to use the option to create a conda environment,
   you need to run **cookiecutter** with Python 3.5+.

Usage
-----

This is a template for the |cc|_ project.

To use it, install |cc|_ and call:

.. code:: bash

   cookiecutter cookiecutter-py




.. |cc| replace:: cookiecutter
.. _cc: https://github.com/audreyr/cookiecutter

